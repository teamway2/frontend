import axios from 'axios';
import {HttpRequest} from "./HttpRequest";

export class HttpService {

    static get(url: string) {
        return new HttpRequest(axios({
            method: 'get',
            url: url,
            timeout: 12000
        }));
    }

    static post(url: string, data:any) {
        return new HttpRequest(axios({
            method: 'post',
            url: url,
            data: data,
            timeout: 12000
        }));
    }

    static put(url: string, data:any) {
        return new HttpRequest(axios({
            method: 'put',
            url: url,
            data: data,
            timeout: 12000
        }));
    }

    static delete(url: string) {
        return new HttpRequest(axios({
            method: 'delete',
            url: url,
            timeout: 12000
        }));
    }
}
