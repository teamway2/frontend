import {AxiosPromise, AxiosRequestConfig, AxiosResponse} from "axios";

export class HttpRequest {

    readonly request: AxiosPromise<AxiosRequestConfig>;

    constructor(request: AxiosPromise<AxiosRequestConfig>) {
        this.request = request;
        this.execute = this.execute.bind(this);
    }

    /**
     * @param resolve
     * @param reject
     */
    async execute(resolve: (response: AxiosResponse<AxiosRequestConfig<any>, any>) => void, reject: (error: any) => void | null) {
        try {
            const response = await this.request;
            resolve(response);
        } catch (error) {
            reject && reject(error);
        }
    }
}

