export const useLocalStorage = () => {
    const set = (key: string, value: string) => localStorage.setItem(key, value);

    const get = (key: string, defaultValue: any) => {
        try{
            return localStorage.getItem(key)
        }catch (e){
            return defaultValue
        }
    }

    const remove = (key: string) => localStorage.removeItem(key);

    return {set,get,remove};
}
