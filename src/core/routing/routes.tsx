import React from 'react';
import {Route} from "../@types/Route";

const Landing = React.lazy(() => import('../../modules/landing/Landing'));
const Test = React.lazy(() => import('../../modules/test/pages/Test'));
const Result = React.lazy(() => import('../../modules/test/pages/Result'));

export const routes: Route[] = [
    {
    path: '/',
    name: '/',
    component: <Landing/>,
    },
    {
        path: '/test',
        name: 'Test',
        component: <Test/>,
    },
    {
        path: '/test/result',
        name: 'result',
        component: <Result/>,
    },


];
