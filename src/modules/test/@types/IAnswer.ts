export interface IAnswer{
    id: number
    label: string,
    score: number
}
