import {IAnswer} from "./IAnswer";

export interface IQuestion {
    id:number
    label: string
    sort: number
    code: string
    answers: IAnswer[]
}
