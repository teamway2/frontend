import React, {useContext, useEffect, useState} from 'react';
import {Answer} from "./Answer";
import {Card} from "../../../shared/components/Card";
import {IAnswer} from "../@types/IAnswer";
import {IQuestion} from "../@types/IQuestion";
import {IAnsweredQuestion} from "../@types/IAnsweredQuestion";
import {QuestionsContext} from "../state/context/questions.context";

type QuestionProps = {
    question: IQuestion
    handleAnswer: (AnsweredQuestion: IAnsweredQuestion) => void
}


export const Question = ({question,handleAnswer}: QuestionProps) => {
    const {state} = useContext(QuestionsContext);
    const {currentQuestionIdx, questions,answeredQuestions} = state;
    const [currentAnswerId, setCurrentAnswerId] = useState<number>(-1);
    useEffect(() => {
        const foundAnswered = answeredQuestions.find((answeredQuestion: IAnsweredQuestion) => answeredQuestion.question_id == question.id);
        setCurrentAnswerId(foundAnswered?.answer_id ?? -1);
    },[currentQuestionIdx])

    const answerQuestion = (answer_id: number) => {
        handleAnswer({
            question_id: question.id,
            answer_id
        });
        setCurrentAnswerId(answer_id);
    }
    return <Card>
        <span className="block font-bold mb-4">{(currentQuestionIdx + 1) + '/' + questions.length} Question</span>
        <h2 className="mb-6">{question?.label}</h2>
        {question && <div>
            {question.answers.map((answer: IAnswer,index:number) => <Answer key={answer.id}
                                                               idx={index + 1}
                                                               checked={answer.id ==currentAnswerId}
                                                               handleChoose={(answerId:number) => answerQuestion(answerId)} answer={answer}/>)}
        </div>}
    </Card>;
}

