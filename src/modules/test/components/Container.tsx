import React,{ReactNode} from "react";


type CardProps = {
    children: ReactNode[] | ReactNode
};

export const Container = ({children} : CardProps) => {
    return <div className="h-full min-h-screen py-3 md:py-0 bg-gradient-to-r from-[#4e7176] to-[#384c6d]  flex justify-center items-center">
        {children}
    </div>
}
