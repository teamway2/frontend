import React from "react";
import {IAnswer} from "../@types/IAnswer";

type AnswerProps = {
    idx: number,
    answer: IAnswer,
    handleChoose: (answerId: number) => void,
    checked: boolean,
}

export const Answer = ({idx,answer,handleChoose,checked} : AnswerProps) => {
    return <div className={(checked ? 'border-solid border-2 border-[#49638F]  ' : '') + 'flex items-center cursor-pointer rounded-md bg-white hover:bg-slate-100 p-3 mt-2'} onClick={() => handleChoose(answer.id)}>
        <span className="px-3 rounded-lg text-white bg-[#49638F]">{idx}</span>
        <label className="ml-2 text-sm font-medium text-gray-900 cursor-pointer">{answer.label}</label>
    </div>
}
