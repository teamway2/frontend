import React, {useContext, useEffect, useMemo, useState} from 'react';
import {QuestionsContext} from "../state/context/questions.context";
import {answerQuestion, changeCurrentQuestionIds, fetchQuestions, submitAnswers} from "../state/actions/action-creators";
import {Question} from "../components/Question";
import {Button} from "../../../shared/components/Button";
import {IAnsweredQuestion} from "../@types/IAnsweredQuestion";
import {ProgressBar} from "../../../shared/components/ProgressBar";
import { useNavigate } from 'react-router-dom';
import {Container} from "../components/Container";
import {Loader} from "../../../shared/components/Loader";


function Test() {
    const {state, dispatch} = useContext(QuestionsContext);
    const {questions, currentQuestionIdx, answeredQuestions,loading,errorMessage,questionsFetched} = state;
    const [enableNext, setEnableNext] = useState(false);
    const [progressWidth, setProgressWidth] = useState(0);
    const navigate = useNavigate();
    const isAllowedIndex = () => {
        return currentQuestionIdx > -1 && currentQuestionIdx < questions?.length;
    }
    useEffect(() => {
        if (!questions.length && !questionsFetched)
            fetchQuestions(dispatch);
        else if (answeredQuestions.length <= questions.length){
            changeCurrentQuestionIds(dispatch, answeredQuestions.length == questions.length ? answeredQuestions.length - 1 : answeredQuestions.length);
            calcProgressBarWidth();
        }
    }, [questions]);

    useMemo(() => {
        if (isAllowedIndex()) {
            const currentQuestion = questions[currentQuestionIdx];
            if(answeredQuestions)
            setEnableNext(answeredQuestions.map((answeredQuestion: IAnsweredQuestion) => answeredQuestion.question_id).indexOf(currentQuestion.id ?? -1) > -1);
        }
    }, [answeredQuestions, currentQuestionIdx]);


    const toNext = () => {
        if (currentQuestionIdx < questions.length - 1) {
            changeCurrentQuestionIds(dispatch, currentQuestionIdx + 1);
            calcProgressBarWidth();
        } else
            submitAnswers(dispatch,answeredQuestions).then(() => {
                navigate('result');
            });
    };
    const toPrev = () => {
        if (currentQuestionIdx > 0)
            changeCurrentQuestionIds(dispatch, currentQuestionIdx - 1);
    };

    const calcProgressBarWidth = () => {
        const completed = answeredQuestions.length - 1 == -1 ? 0 : answeredQuestions.length;
        const total = questions.length ?? 0;
        setProgressWidth((completed / total) * 100);
    }


    return <Container>
        {loading ? <Loader /> : (isAllowedIndex() ?
            <div className="w-11/12 md:w-9/12 lg:w-2/5 ">
                <div className="mb-6">
                    <ProgressBar width={progressWidth}/>
                </div>
                <Question question={questions[currentQuestionIdx]}
                          handleAnswer={(answeredQuestion: IAnsweredQuestion) => answerQuestion(dispatch, answeredQuestion)}/>
                <div className="flex justify-center width-full pb-[4rem]">
                    <Button label={'Previous'} classes={'w-5/12 mr-3 bg-[#384c6d] hover:bg-[#384c60] font-bold '} handleClick={toPrev}/>
                    <Button label={currentQuestionIdx == questions.length - 1 ? 'Show Result' : 'Next'} classes={'w-5/12 bg-[#384c6d] hover:bg-[#384c60] font-bold disabled:bg-gray-400'} disabled={!enableNext}
                            handleClick={toNext}/>
                </div>
            </div> : <span className="font-bold red">{errorMessage}</span>)}

    </Container>;
}


export default Test;
