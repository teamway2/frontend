import React, {useContext} from 'react';
import {Container} from "../components/Container";
import {QuestionsContext} from "../state/context/questions.context";
import {Card} from "../../../shared/components/Card";
import {Link} from "react-router-dom";

function Result() {
    const {state} = useContext(QuestionsContext);
    const {result} = state;
    return <Container>
        <Card classes={"p-6 text-center w-11/12 md:w-9/12 lg:w-2/5"}>
                <h3 className="font-bold">{result ? 'Congratulations You completed the test Successfully ' : 'You didn\'t complete the test '}</h3>
                {result ? <h5 className="font-bold">Your test result is </h5> : <h5 className="mt-5 underline underline-offset-2"><Link to={"/test"}>Back to your test</Link></h5>}
                <div className="mt-8">
                    <strong className="text-[#4C6D38]">{result}</strong>
                </div>
                {result && <h5 className="mt-5 underline underline-offset-2"><Link to={"/test"}>Retake the test</Link></h5>}
        </Card>
    </Container>;
}


export default Result;
