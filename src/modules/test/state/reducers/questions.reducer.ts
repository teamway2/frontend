import {Action} from "../actions";
import {ActionTypes} from "../actions/action-types";
import {ContextState, StateType} from "../interfaces/state";
import {IAnsweredQuestion} from "../../@types/IAnsweredQuestion";
import {useLocalStorage} from "../../../../core/hooks/useLocalStorage";
import {ANSWERED_QUESTIONS, TEST_RESULT} from "../../constants/constants";


const questionsReducer = (state: StateType = ContextState, action: Action) => {
    const {set,remove} = useLocalStorage();
    switch(action.type){
        case ActionTypes.FETCHING_QUESTIONS:
            return {...state, loading: true}
        case ActionTypes.FETCHING_QUESTIONS_SUCCESSFULLY:{
            const questions = action.payload.questions;
            const currentQuestionIdx =0;
            remove(TEST_RESULT); // clear the old result
            return {...state, questions,currentQuestionIdx,loading: false,questionsFetched:true, result:''}
        }
        case ActionTypes.ANSWER_QUESTION:{
            const answeredQuestion = action.payload.answeredQuestion;
            let {answeredQuestions} = state;
            answeredQuestions = answeredQuestions.filter((item: IAnsweredQuestion) => item.question_id != answeredQuestion.question_id);
            answeredQuestions = [...answeredQuestions, answeredQuestion];
            set(ANSWERED_QUESTIONS, JSON.stringify(answeredQuestions));
            return {...state, answeredQuestions }
        }
        case ActionTypes.CHANGE_CURRENT_QUESTION_IDX:{
            return {...state, currentQuestionIdx: action.payload.newIdx}
        }
        case ActionTypes.SUBMIT_ANSWERS:{
            return {...state, loading: true}
        }
        case ActionTypes.GETTING_RESULT:{
            const result = action.payload.result;
            set(TEST_RESULT, result);
            remove(ANSWERED_QUESTIONS);
            return {...state,answeredQuestions:[],questions:[],questionsFetched:false, result}
        }
        case ActionTypes.AXIOS_ERROR:{
            return {...state, loading: action.payload.loading, errorMessage: action.payload.errorMessage}
        }
        default:
            return state;
    }
}

export default questionsReducer;
