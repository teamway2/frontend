import {Action} from "./index";
import {ActionTypes} from "./action-types";
import {Dispatch} from "react";
import {HttpService} from "../../../../core/http/http.service";
import {BASE_API_URL} from "../../../../core/constants/api-url";
import {Response} from '../../../../shared/@types/Response'
import {IAnsweredQuestion} from "../../@types/IAnsweredQuestion";

export const  fetchQuestions = (dispatch: Dispatch<Action>) => {
    dispatch({
        type: ActionTypes.FETCHING_QUESTIONS,
    });
    HttpService.get(BASE_API_URL + 'questions').execute((response: any) => {
        dispatch({
            type: ActionTypes.FETCHING_QUESTIONS_SUCCESSFULLY,
            payload: {questions: response.data.data, loading:false}
        })
    }, (error: Response) => {
        console.error(error);
        dispatch({
            type: ActionTypes.AXIOS_ERROR,
            payload: {loading:false, errorMessage: "sorry we have a problem please try again"}
        })
    });

}

export const changeCurrentQuestionIds = (dispatch: Dispatch<Action>, newIdx: number) => {
    dispatch({type: ActionTypes.CHANGE_CURRENT_QUESTION_IDX, payload: {newIdx}})
}

export const answerQuestion = (dispatch: Dispatch<Action>, answeredQuestion: IAnsweredQuestion) => {
    dispatch({
        type: ActionTypes.ANSWER_QUESTION,
        payload: {answeredQuestion}
    })
}
export const submitAnswers = (dispatch: Dispatch<Action>, answeredQuestions: IAnsweredQuestion[]) => {
    dispatch({
        type: ActionTypes.SUBMIT_ANSWERS,
    });
    return HttpService.post(BASE_API_URL + 'submit', {answered_questions: answeredQuestions}).execute((response: any) => {
        dispatch({
            type: ActionTypes.GETTING_RESULT,
            payload: {result: response.data.data.result}
        })
    }, (error: Response) => {
        console.error(error);
    });
}
