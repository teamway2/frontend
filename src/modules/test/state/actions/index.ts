import {ActionTypes} from './action-types'
import {IQuestion} from "../../@types/IQuestion";
import {IAnsweredQuestion} from "../../@types/IAnsweredQuestion";


interface FetchingQuestion {
    type: ActionTypes.FETCHING_QUESTIONS,
}

interface FetchingQuestionsSuccessfully {
    type: ActionTypes.FETCHING_QUESTIONS_SUCCESSFULLY,
    payload: {
        questions: IQuestion[],
        loading: boolean
    }
}


interface AnswerQuestion {
    type: ActionTypes.ANSWER_QUESTION,
    payload: {
        answeredQuestion: IAnsweredQuestion,
    }
}

interface ChangeCurrentQuestionIdx {
    type: ActionTypes.CHANGE_CURRENT_QUESTION_IDX,
    payload: {
        newIdx: number,
    }
}

interface SubmitAnswers {
    type: ActionTypes.SUBMIT_ANSWERS,
}

interface GettingResult {
    type: ActionTypes.GETTING_RESULT,
    payload: {
        result: string,
    }
}

interface AxiosError {
    type: ActionTypes.AXIOS_ERROR,
    payload: {
        loading: boolean
        errorMessage: string
    }
}

export type Action =
    FetchingQuestion
    | FetchingQuestionsSuccessfully
    | AnswerQuestion
    | ChangeCurrentQuestionIdx
    | SubmitAnswers
    | GettingResult
    | AxiosError;
