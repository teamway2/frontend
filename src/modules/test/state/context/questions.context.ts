import {createContext, Dispatch} from "react";
import {Action} from "../actions";
import {StateType} from "../interfaces/state";

interface QuestionsContextProps {
    state: StateType;
    dispatch: Dispatch<Action>
}

export const QuestionsContext = createContext<QuestionsContextProps>({} as QuestionsContextProps)
