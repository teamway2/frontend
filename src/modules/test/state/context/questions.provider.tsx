import React, {FC, ReactNode, useReducer} from "react";
import {ContextState} from "../interfaces/state";
import {QuestionsContext} from "./questions.context";
import questionsReducer from "../reducers/questions.reducer";

interface Props {
    children: ReactNode
}

export const QuestionsProvider : FC<Props> = ({children}) =>{
    const [state, dispatch] = useReducer(questionsReducer, ContextState)

    return (
        <QuestionsContext.Provider value={{state, dispatch}}>
            {children}
        </QuestionsContext.Provider>
    )
}
