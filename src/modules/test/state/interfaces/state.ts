import {IAnsweredQuestion} from "../../@types/IAnsweredQuestion";
import {IQuestion} from "../../@types/IQuestion";
import {useLocalStorage} from "../../../../core/hooks/useLocalStorage";
import {ANSWERED_QUESTIONS, TEST_RESULT} from "../../constants/constants";

const {get} = useLocalStorage();
export interface StateType {
    questions: IQuestion[],
    questionsFetched: boolean,
    loading : boolean,
    answeredQuestions: IAnsweredQuestion[],
    currentQuestionIdx: number,
    result: string,
    errorMessage: string,
}


export const ContextState: StateType ={
    questions: [],
    questionsFetched: false,
    loading: false,
    answeredQuestions: get(ANSWERED_QUESTIONS,[]) ? JSON.parse(get(ANSWERED_QUESTIONS,[])) : [],
    currentQuestionIdx: -1,
    result: get(TEST_RESULT,null) ?? null,
    errorMessage: 'Sorry we have a problem please try again',
}
