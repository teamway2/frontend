import React, {useContext} from 'react';
import {Link} from "react-router-dom";
import "../../core/styles/pages/landing.scss";
import {Button} from "../../shared/components/Button";
import {QuestionsContext} from "../test/state/context/questions.context";

function Landing() {
    const {state} = useContext(QuestionsContext);
    const {answeredQuestions,result} = state;
    return <div className="landing-container flex items-center bg-center bg-cover h-screen">
        <div className="overlay"/>
        <div className="lg:ml-20 lg:text-left text-center max-w-xl z-10">
            <span className="px-8 md:px-0 block text-blue-700 font-bold text-2xl md:text-4xl font-mono text-slate-50 leading-loose">
            Would you want to know how do you look in Social life?
            </span>
            <div className="flex justify-around mt-5">
                <Link to={'test'}>
                    <Button classes={'bg-fuchsia-800 hover:bg-fuchsia-900 font-mono !text-slate-300 !rounded-[6rem] max-w-[90%] px-3 py-2 leading-none'}
                            label={answeredQuestions.length > 0 ? 'Complete Your test' : 'Start your test'}/>
                </Link>
                {result && <Link to={'test/result'}>,,
                    <Button classes={'ml-3 bg-fuchsia-300 hover:bg-fuchsia-400 !text-slate-900 font-mono !rounded-[6rem] max-w-[90%] px-3 py-2 leading-none '} label={'Show your last result'}/>
                </Link>}
            </div>
        </div>
    </div>;
}

export default Landing;
