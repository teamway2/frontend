import React,{Suspense} from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import './core/styles/styles.scss';
import reportWebVitals from './reportWebVitals';
import ErrorBoundary from "./core/components/ErrorBoundary";
import {BrowserRouter, Route,Routes} from "react-router-dom";
import {routes} from "./core/routing/routes";
import {QuestionsProvider} from "./modules/test/state/context/questions.provider";
import {Footer} from "./shared/layouts/Footer";

const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
);
root.render(
    <React.StrictMode>
        <ErrorBoundary>
            <QuestionsProvider>
                <BrowserRouter>
                    <Suspense>
                        <Routes>
                            {routes.map((route,index:number) => <Route key={index} path={route.path} element={route.component}/>)}
                        </Routes>
                    </Suspense>
                </BrowserRouter>
                <Footer />
            </QuestionsProvider>
        </ErrorBoundary>
    </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
