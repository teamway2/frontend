export interface Response {
    status: string
    code: number
    data: Array<{}>
    message: string
}
