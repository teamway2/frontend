import React,{ReactNode} from "react";


type CardProps = {
    children: ReactNode[] | ReactNode,
    classes?: string
};

export const Card = ({children,classes} : CardProps) => {
   return <div className={(classes ?? "") + " bg-slate-200 rounded-lg shadow-lg p-4"}>
       {children}
   </div>
}
