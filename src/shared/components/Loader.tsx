import React from "react";
import "../../core/styles/components/_loader.scss";

export const Loader = () => {
    return <div className="loader">
        <div className={"loader__elem loader__elem--1"} />
        <div className={"loader__elem loader__elem--2"} />
        <div className={"loader__elem loader__elem--3"} />
        <div className={"loader__elem loader__elem--4"} />
    </div>
}
