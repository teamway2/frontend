import React from 'react';

type ProgressBarProps = {
    width: number,
}
export const ProgressBar = ({width}: ProgressBarProps) => {
    return (
        <div className="w-full bg-gray-200 rounded-full">
            <div className="bg-[#49638F] text-xs font-medium text-blue-100 text-center p-0.5 leading-none rounded-full transition transition-[width] delay-200 " style={{width: width + '%'}} >
                {width + '%'}
            </div>
        </div>
    )
}
