import React from "react";

type ButtonProps = {
    label: string
    classes?: string
    disabled?: boolean
    handleClick?: () => void
};
export const Button = ({label, classes, handleClick, disabled}: ButtonProps) => {
    return <button type="button"
                   disabled={disabled ?? false}
                   className={(classes ?? "") + " p-3 rounded-lg mt-6 text-white rounded-[6rem] "}
                   onClick={handleClick}>{label}</button>
}
