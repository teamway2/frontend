import React from "react";



export const Footer = () => {
    return <footer
        className="p-1 z-50 fixed bottom-0 w-full bg-white  shadow md:flex md:items-center md:justify-between md:p-2 dark:bg-gray-800">
    <span className="text-sm text-gray-500 sm:text-center dark:text-gray-400">Made with love By: &nbsp;
        <a href="mailto:ali.alajouz.995@gmail.com">ali.alajouz.995@gmail.com</a>
    </span>
    </footer>
}
